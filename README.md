# README #

### Nice2Predict-Language-Support ###

Nice2Predict is a language-independent framework to deduce program properties. However, to use Nice2Predict, the source code should be converted into an intermediate JSON format that lists the properties of the input source code.

Nice2PredictLanguageSupport currently builds features from input source files such that they are compatible with Nice2Predict. Since this repository internally uses Nice2Predict, there is no need to pull and build this code separately. 

*** Supported Languages: Python, Javascript.***

* Version: [beta]1.0
* See [here](https://github.com/eth-srl/Nice2Predict) for a summary of Nice2Predict does

### The knitty gritties ###
As an end user, post setup, your access point to the server will be available at http://localhost:5000 by default. Once you choose the appropriate language and input the source file, the server will then convert them into the corresponding features. 

These features are in turn fed to the Nice2Predict server hosted internally, which will provide valid and safe modification suggestions in your input source code. These modifications and the input source code are then used to determine the valid, cleaned up code. 

A fully functional sample server is available at http://54.227.199.108:5000 for you to play around.

### Setup Instructions ###
The meat of the setup is done by setup.sh. It takes your ssh private keys as input and sets up the entire environment. 

#### 0. Optional training ####
This repository has pretrained data for all the supported languages. *So feel free to skip this step, if required* If you have better source codes, we recommend this step though. 

Choose the appropriate location from <cloned_repo>/train/<language>/update_train depending on the input language. Create a new directory <test_dir> and place all the sample files for training into this new directory and execute the command: 

```
cd <cloned_repo>/train/<language>/update_train 
python extracted_features.py --dir <test_dir>  > ../bulk/<training_file_name>
```
Commit it into your private clone and then use the same. 

#### 1. Generate SSH keys ####
Depending on your server's environment follow the steps available [in this page](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html). In a Linux box, you will get two keys: 
* id_rsa - the private key
* id_rsa.pub - the public key
Copy the fully qualified path of the **private key**

#### 2. Setup the environment ####
A lot goes on here. Run these commands:

```
cd <cloned_repo>/install
sudo -H ./setup.sh <private_key_fully_qualified_name>
```
That's about the entire setup. This will pull all required dependent libraries and set the rest of the environment. 

#### 3. Start the Flask server ####
The setup.sh doesn't start the Flask server. We recommend the following steps in a Linux box to start the server at http://localhost:5000

```
cd <cloned_repo>/src/server/
screen flask_n2p

#Within the screen
python app.py 

#Use Ctrl+A, d to exit the screen while the process still runs
```

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact