#!/bin/bash

cur_dir=`pwd`
cd $cur_dir
ls -l *.json
echo "Verify if the required json files are present. Press any key to continue"
read -n 1 -s 

temp_dir=$(mktemp -d)
temp_op_dir=$(mktemp -d)
echo "Made temporary directories at $temp_dir and $temp_op_dir"
idi=1

#"test1", "test2"
for fl in "given"
do
  for meth in "showgraph" "infer"
  do
    req="{\"jsonrpc\" :\"2.0\", \"method\" : \"$meth\", \"id\" : $idi, \"params\": `cat $fl.json`}"
    dmp_file="$fl$meth.json"
    op=$temp_dir/$dmp_file
    echo "Dumping request to $op"
    echo $req > $op
    echo "Request in $op will be executed. Output is at $temp_op_dir/$dmp_file"
    postq="curl -sS -X POST -d @$op http://localhost:8080 --header \"Content-Type:application/json\" > $temp_op_dir/$dmp_file"
    eval $postq
    idi=$((idi+1))
  done
done

echo "Press any key to proceed to delete request files..."
read -n 1 -s 
rm -rf $temp_dir

cat $temp_op_dir/giveninfer.json
echo "Verify if the above output are correct and then press a key! "
read -n 1 -s 
