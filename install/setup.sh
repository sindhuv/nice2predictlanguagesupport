#!/bin/bash
apt-get update && apt-get install screen docker python-pip python-dev redis-server nodejs nodejs-legacy
pip install flask astor redis rq

cur_dir=`pwd`
cd n2p_py
cp $1 ./id_rsa
docker build -t n2p_python --no-cache=true . 
docker run -itd -p 0.0.0.0:8085:5746 n2p_python
rm ./id_rsa

cd ../n2p_js
cp $1 ./id_rsa
docker build -t n2p_javascript --no-cache=true .
docker run -itd -p 0.0.0.0:8086:5745 n2p_javascript
rm ./id_rsa

cd ../../src/server
python worker.py &
cd $cur_dir

