import commands
import tempfile, os, json, sys, random, traceback, re, pickle
import StringIO

#TODO: Update to correct path later
scriptpath = "../n2p_python"
sys.path.append(os.path.abspath(scriptpath))
from feature_extractor import *
from clean_code import *

JAVASCRIPT = 0
PYTHON = 1

JS_PORT = 8086
PYTHON_PORT = 8085

port_map = { JAVASCRIPT : JS_PORT, PYTHON : PYTHON_PORT } 

js_command_fs = 'unuglifyjs %s --extract-features'
js_rename_command_fs = 'unuglifyjs %s --rename'

curl_command = """curl -sS -X POST -d @%s http://localhost:%d --header "Content-Type:application/json" """

get_redis_key = lambda sl, code : str(sl)+"=="+code

def update_unicode_dict(feature_dict):
	fstrkey_dict = {"query" : [], "assign" : []}
	qarr = []
	aarr = []
	for arr in feature_dict["query"]:
		opdict = {}
		for k, v in arr.items():
			k1, v1 = k, v
			if type(k) == unicode:
				k1 = k.encode()
			if type(v) == unicode:
				v1 = v.encode()
			opdict[k1] = v1
		qarr.append(opdict)
	fstrkey_dict["query"] = qarr

	for arr in feature_dict["assign"]:
		opdict = {}
		for k, v in arr.items():
			k1, v1 = k, v
			if type(k) == unicode:
				k1 = k.encode()
			if type(v) == unicode:
				v1 = v.encode()
			opdict[k1] = v1
		aarr.append(opdict)
	fstrkey_dict['assign'] = aarr
	return fstrkey_dict

def retrieve_cached_features(src_lang, code, redis_cache):
	rkey = get_redis_key(src_lang, code)
	errors = ""
	op = None

	packed = redis_cache.get(rkey)
	if packed is None:
		print "Nothing in cache"
		op, errors = retrieve_features_generic(src_lang, code)
		if not errors == "":
			return "", errors
		unpacked = pickle.dumps(op)
		redis_cache.setex(rkey, 86400, unpacked)
	else:
		op = pickle.loads(packed)

	features = dict()
	if src_lang == JAVASCRIPT:
		features = op
	elif src_lang == PYTHON:
		tree, features, symbols = op[0], op[1], op[2]
	
	return json.dumps(features, indent = 2), errors

def retrieve_features_generic(src_lang, code):
	try:
		if src_lang == JAVASCRIPT:
			tmp = tempfile.NamedTemporaryFile(delete = True)
			try:
				tmp.write(code)
				tmp.flush()
				op = commands.getstatusoutput(js_command_fs % tmp.name)

				if op[0] != 0:
					return "", op[1]

				if op[1].lower().startwith("warn"):
					js_data = """{"query": [], "assign" : []}"""
				else:
					js_data = op[1].replace('\'', '"')

				js_data = op[1].replace('\'', '"')
				feature_dict = json.loads(js_data)
				fstrkey_dict = update_unicode_dict(feature_dict)
				return fstrkey_dict, ""
			finally:
				tmp.close()
		elif src_lang == 1:
			output = dict()
			fe = FeatureExtractor(code)
			fe_op = fe.extract()
			return fe_op, ""
		else:
			return "", "Language not supported. Try with Python only"
	except SyntaxError:
		return "", "Syntax exceptions raised. Check for compilation errors in %s" % code
	except:
		return "", "Unhandled exception. Please try later"
		

def get_modifications(src_lang, features_str):
	try:
		features_str = features_str.replace("'", '"')
		features_str = re.sub('\s+', ' ', features_str)
		features = json.loads(features_str)

		n2p_request = dict()
		n2p_request["jsonrpc"] = "2.0"
		n2p_request["method"] = "infer"
		n2p_request["id"] = random.randint(0, 1000)
		n2p_request["params"] = features
		n2p_output = dict()

		tmp = tempfile.NamedTemporaryFile(delete = True)

		n2p_request_data = json.dumps(n2p_request)
		n2p_request_data = n2p_request_data.replace('\'', '"')
		tmp.write(n2p_request_data)
		tmp.flush()
		n2p_output = commands.getstatusoutput(curl_command %(tmp.name, port_map[src_lang]))
		if n2p_output[0] != 0:
			return "", n2p_output[1]

		try:
			output = json.loads(n2p_output[1])
			return json.dumps(output, indent = 2), ""
		except:
			return "", "Nice2Predict framework down. Retry after starting n2p server."
	except:
		return "", "Input features not valid. Retry"
	finally:
		tmp.close()

def modify_output(src_lang, code, modifications, redis_cache):
	try:
		rkey = get_redis_key(src_lang, code)
		packed = redis_cache.get(rkey)
		if packed is None:
			return "", "Mapping cached only for a day. No details in the cache for the given code. Try again from scratch"

		if src_lang == PYTHON:
			op = pickle.loads(packed)
			modif_json = json.loads(modifications)
			modified_code = update_code(op[0], op[1], op[2], modif_json)
			return modified_code, ""

		if src_lang == JAVASCRIPT:
			tmp = tempfile.NamedTemporaryFile(delete = True)
			tmp.write(code)
			tmp.flush()
			op = commands.getstatusoutput(js_rename_command_fs % tmp.name)
			if op[0] != 0:
				return "", op[1]
			return output, ""
	except:
		return "", "Unable to modify the output now. Please try later"
