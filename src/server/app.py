#TODO: Logger

from flask import Flask, render_template, request, session, flash
import logging
from logging.handlers import TimedRotatingFileHandler
import sys, json
sys.path.append(".")
from n2p_utils import * 

from redis import StrictRedis

SECRET_KEY = "d4a1d474d581251377214a6a5a145be1d275289a99f4c96d"
app = Flask(__name__)
app.secret_key = SECRET_KEY
#q = Queue(connection=conn)

"""
app.config.from_object(__name__)
Session(app)
"""

debug = True #Set to False in production envs.

languages = dict()
languages[0] = 'Javascript'
languages[1] = 'Python'

redis_url = os.getenv('REDISTOGO_URL', 'redis://localhost:6379')
redis_cache = StrictRedis.from_url(redis_url)

@app.route('/')
def intro_page():
	return render_template('init_page.html')

@app.route('/nice2predict')
def get_supported_languages():
	return render_template('language_list.html', languages = languages)

@app.route('/nice2predict/<int:src_lang>')
def show_page(src_lang):
	return render_template('src_code.html', src_lang = src_lang, code = "", features = "", modifications = "", modified_op = "")

@app.route('/nice2predict/<int:src_lang>/extract', methods = ['POST'])
def extract_features(src_lang):
	code = request.form["src_code_content"]
	features_str, errors = retrieve_cached_features(src_lang, code, redis_cache)

	is_incomplete = errors and len(errors)>0
	if is_incomplete:
		flash(errors, 'error')
	return render_template('extract.html', src_lang = src_lang, code = code, features = features_str, modifications = "", modified_op = "", errors = is_incomplete)

@app.route('/nice2predict/<int:src_lang>/map', methods = ['POST'])
def infer(src_lang):
	code = request.form["src_hidden"]
	features = request.form["features_content"]
	modifications, errors = get_modifications(src_lang, features)

	is_incomplete = errors and len(errors)>0
	if is_incomplete:
		flash(errors, 'error')
	return render_template('infer.html', src_lang = src_lang, code = code, features = features, modifications = modifications, modified_op = "", errors = is_incomplete)

@app.route('/nice2predict/<int:src_lang>/modify', methods = ['POST'])
def modify_src_code(src_lang):
	code = request.form["src_hidden"]
	features = request.form["features_hidden"]
	modifications = request.form["infer_content"]
	modified_op, errors = modify_output(src_lang, code, modifications, redis_cache)

	is_incomplete = errors and len(errors)>0
	if is_incomplete:
		flash(errors, 'error')

	return render_template('modified_op.html', src_lang = src_lang, code = code, features = features, modifications = modifications, modified_op = modified_op, errors = is_incomplete)

if __name__ == '__main__':
	redis_cache.flushall()
	#app.run(host='0.0.0.0')
	app.run(debug = True)
