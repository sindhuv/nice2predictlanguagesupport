import astor, ast
import pickle, json

def update_code(tree, features, symbols, inferred_details):
	ipf = features["assign"]
	opf = inferred_details["result"]

	ip_dict = {}
	for ip in ipf:
		if 'inf' in ip.keys():
			ip_dict[ip['v']] = ip['inf']

	op_dict = {}
	for op in opf:
		if 'inf' in op.keys():
			op_dict[op['v']] = op['inf']

	for k in op_dict.keys():
		tree = update_tree(tree, symbols[k], ip_dict[k], op_dict[k])

	code = astor.to_source(tree)
	return code

def update_tree(node, symbol_prop, old_value, new_value):
	if node != symbol_prop.parent:
		for child in ast.iter_child_nodes(node):
			update_tree(child, symbol_prop, old_value, new_value)
	else:
		for modable in ast.walk(node):
			if isinstance(modable, ast.Name) and modable.id == old_value:
				modable.id = new_value
	return node


#op = pickle.load(open('sample.pkl', 'rb'))
#inferred =  json.loads("""{"id":2,"jsonrpc":"2.0","result":[{"giv": "self", "v": 0} , {"inf": "n1", "v": 1} , {"inf": "n2", "v": 2} , {"inf": "is_greater_than", "v": 3} ,{"giv": "a", "v": 4} , {"giv": "b", "v": 5} , {"giv": "self", "v": 6} , {"giv": "__init__", "v": 7} , {"giv": "blah", "v": 8} , {"giv": "A", "v": 9}]}""")
#update_code(op[0], op[1], op[2], op[3], inferred)
