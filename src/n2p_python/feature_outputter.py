from n2p_features_common import *
class FeatureOutputter:
	def __init__(self, unique_node_map, debug):
		self.features = self.clean_n2p_dict()
		self.unique_node_map = unique_node_map
		self.debug = debug
		self.unique_symbols = list()

	def clean_n2p_dict(self):
		dc = dict()
		dc["query"] = []
		dc["assign"] = []
		return dc

	def add_feature(self, np1, np2, feature):
		if np1.name == "" or np2.name == "":
			return

		if np1.annotation != "":
			feature = np1.annotation+"-"+feature
		if np2.annotation != "":
			feature = feature + "-"+np2.annotation
		a_idx = self.update_get_symbol_index(np1)
		b_idx = self.update_get_symbol_index(np2)
		if not np1.must_infer and not np2.must_infer:
			return
		if a_idx == b_idx:
			return
		query = dict()
		query["f2"] = feature
		query["a"] = a_idx
		query["b"] = b_idx
		if query not in self.features["query"]:
			self.features["query"].append(query)

	def add_scope(self, arr):
		scope_dict = dict()
		scope_dict["cn"] = "!="
		scope_dict["n"] = [self.unique_symbols[i].name for i in arr]
		self.features["query"].append(scope_dict)

	def generate_assignments(self):
		for i in range(len(self.unique_symbols)):
			agn = dict()
			agn["v"] = i
			mi = "inf" if self.unique_symbols[i].must_infer else "giv"
			agn[mi] = self.unique_symbols[i].name
			self.features["assign"].append(agn)

	def add_multiple_features(self, n1s, lhs_label, n2, rhs_label):
		lhss = [get_property(n, self.unique_node_map[n]) for n in n1s]
		nname = validate_node(n2)
		if nname != "":
			rhs = get_property(n2, self.unique_node_map[n2])
			prefix = ""
			for i in range(len(lhss)-1, -1, -1):
				prefix += lhs_label;
				self.add_feature(lhss[i], rhs, prefix + rhs_label)
		else:
			if self.debug:
				print "No valid right node. Ignore if it is return statement / list ", n2

	def update_output_dict(self, n1, n2, bw_path, fw_path, cp_len):
		if n1 is None or n2 is None:
			return
		feature = self.path_to_string_bw(bw_path, cp_len)+":"+self.path_to_string_fw(fw_path, cp_len-1)
		np1 = get_property(n1, self.unique_node_map[n1])
		np2 = get_property(n2, self.unique_node_map[n2])

		self.add_feature(np1, np2, feature)

	def get_child_id(self, node):
		if node in self.unique_node_map.keys():
			return str(self.unique_node_map[node][0])
		return "0"

	def path_to_string_fw(self, path, start):
		res = ""
		for i in range(start, len(path)-1):
			res += str(type(path[i]))[13:-2]
			op = self.get_child_id(path[i+1])
			res += "["+op+"]" 
		return res

	def path_to_string_bw(self, path, start):
		op = self.get_child_id(path[-1])
		res = "["+op+"]" 
		for i in range(len(path)-2, start, -1):
			res += str(type(path[i]))
			op = self.get_child_id(path[i])
			res += "["+op+"]"
		return res

	def update_get_symbol_index(self, prop):
		for i in range(len(self.unique_symbols)):
			if self.unique_symbols[i].name == prop.name and self.unique_symbols[i].parent == prop.parent:
				if self.debug:
					print i, ":", prop.to_string()," == ", self.unique_symbols[i].to_string()
				return i
		self.unique_symbols.append(prop)
		return len(self.unique_symbols)-1

	def generate_scope_relations(self, scoped_node):
		arr = set()
		for node in ast.walk(scoped_node):
			if hasattr(node, 'name') or (isinstance(node, ast.Name) and node.id not in ["True", "False", "None"]):
				#print "Scoping ", scoped_node, " Found node ", node.id if hasattr(node, 'id') else node.name, " PArent = ", self.unique_node_map[node][1]
				if self.unique_node_map[node][1] == scoped_node:
					prop = get_property(node, self.unique_node_map[node])
					idx = self.update_get_symbol_index(prop)
					arr.add(idx)
		self.add_scope(sorted(arr))
