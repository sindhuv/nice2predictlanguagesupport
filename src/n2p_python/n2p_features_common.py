import ast, re

## PROPERTY UTILS
class Property:
	def __init__(self, must_infer, name, annotation, node):
		self.must_infer = must_infer
		self.name = name
		self.annotation = annotation
		self.parent = node#load nodes
	def to_string(self):
		return self.name+"-"+str(self.must_infer)+"-"+self.annotation+"-"+str(self.parent)

def get_property(node, pnode_arr):
	#ctx is the key
	_, logical_pnode, actual_pnode = pnode_arr
	nname = validate_node(node)
	if nname is None:
		print "BLAHISH ", nname, node, node.__dict__
	must_infer = is_inferrable(node, logical_pnode, actual_pnode, nname)
	node_class = node.__class__.__name__
	annotation = get_annotation(node, nname) 
	prop = Property(must_infer, nname, annotation, logical_pnode)
	return prop

def get_annotation(node, nname):
	if nname in ["True", "False"]:
		return "Bool"
	if nname in ["self", "None"]:
		return nname
	if nname == "_" or (nname.startswith("__") and nname.endswith("__")):
		return "Fixed"
	if node.__class__.__name__ in ["Name", "Attribute", "arguments", "FunctionDef", "ClassDef", "Lambda", "ExceptHandler"]:
		return ""
	return node.__class__.__name__

def is_inferrable(node, logical_pnode, actual_pnode, nname):
	if isinstance(actual_pnode, ast.Call):
		return False
	if isinstance(logical_pnode, ast.Import):
		return False
	if nname in ["True", "False", "None", "self"] or nname == "_" or (nname.startswith("__") and nname.endswith("__")):
		return False
	if node.__class__.__name__ == "Attribute":
		return False
	if hasattr(node, 'col_offset') and node.col_offset == 0:
		return False
	if not hasattr(node, 'ctx'):
		return False
	return True

def validate_node(node):
	if type(node) == type([]):
		return ""
	nname = ""

	valid_names = ['id', 'attr', 's', 'n', 'name']

	if hasattr(node, 'id'):
		nname = node.id
	elif hasattr(node, 'attr'):
		nname = node.attr
	elif hasattr(node, 's'):
		nname = node.s
	elif hasattr(node, 'n'):
		nname = str(node.n)
	elif hasattr(node, 'name'):
		nname = node.name

	if nname is None or type(nname) != str:
		children = [x for x in ast.iter_child_nodes(node)]
		for child in children:
			nname = validate_node(child)
			if not nname is None and not nname == "":
				break
	#print node.__dict__, nname
	return nname

## NODE UTILS
def get_id(func_node):
	if hasattr(func_node, 'id'):
		return func_node.id, True
	if hasattr(func_node, 'value'):
		return get_id(func_node.value), True
	if hasattr(func_node, 'func'):
		return get_id(func_node.func), True
	return "", False

def is_node_from_import(node, import_keys):
	if not isinstance(node, ast.Call):
		return False
	if not hasattr(node.func, 'value'):
		return False
	if isinstance(node.func.value, ast.Attribute):
		return False
	node_id, is_valid = get_id(node.func)
	if not is_valid:
		print "OO : ", [x.__dict__ for x in ast.walk(node)]
	return node_id in import_keys

def generate_node_map(tree):
	#Two-pass generation
	unique_node_map = dict()
	unique_node_map[tree] = [0, tree, tree]
	import_names = dict()
	bfw = ast.walk(tree)

	for node in bfw:
		child_nodes = [x for x in ast.iter_child_nodes(node)]
		for i in range(len(child_nodes)):
			#print child_nodes[i], child_nodes[i].__dict__, "\n"
			"""			
			if hasattr(child_nodes[i], 'func'):
				print "==", [[x, x.__dict__] for x in ast.walk(child_nodes[i])]
			"""
			actual_parent = node
			scope_parent = node
			if isinstance(node, ast.Import):
				for imp in ast.walk(node):
					if isinstance(imp, ast.alias): 
						import_names[imp.name] = imp
						if imp.asname:
							import_names[imp.asname] = imp

			if node.__class__.__name__ in ["Module", "FunctionDef", "Lambda", "ClassDef"]:
				if hasattr(node, 'name'):
					scope_parent = node
				else:
					scope_parent = unique_node_map[node][2]
			elif is_node_from_import(child_nodes[i], import_names.keys()):
				scope_parent = import_names[get_id(child_nodes[i].func)]
			else:
				scope_parent = unique_node_map[node][1]
			unique_node_map[child_nodes[i]] = [i, scope_parent, actual_parent]

	for k, v in unique_node_map.items():
		updated_parent = v
		if isinstance(v[1], ast.arguments):
			updated_parent = unique_node_map[v[1]][1]
			unique_node_map[k] = [v[0], updated_parent, v[2]]
	return unique_node_map

def is_searchable(node):
	return not hasattr(node, 'body') #node.__class__.__name__ == "Call" or 

def search_paths(node, cur_depth, max_depth = 3):
	unroot = ["List", "Add", "Call"] #TODO: Keep adding ignorable list
	searchable = is_searchable(node)

	if cur_depth == max_depth or not searchable:
		return []

	op = ast.iter_child_nodes(node)
	child_nodes = [x for x in op]
	root = node

	rooted_paths = [] if node.__class__.__name__ in unroot else [[root]] 
	for subtree in child_nodes:
		if  not subtree.__class__.__name__ in ["Load", "Store", "Param"]:
			sub_paths = search_paths(subtree, cur_depth + 1)
			for path in sub_paths:
				rooted_paths.append([root] + path)
	return rooted_paths
