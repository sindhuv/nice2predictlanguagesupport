# TODO
# 1. Scope constraints for class??
# 2. Enclosed & Global scopes fill

import ast
from feature_outputter import *
import logging

OP_TREE = 0
OP_FEATURES = 1
OP_SYMBOLS = 2

class FeatureExtractor:
	def __init__(self, code, debug = False):
		self.code = code
		self.tree = ast.parse(code)
		self.feature_outputter = FeatureOutputter(generate_node_map(self.tree), debug)
		self.debug = debug


	def extract(self, extractables = ["ASTREL", "FNAMES", "FSCOPE"]):
		self.feature_outputter.clean_n2p_dict()
		self.generate_features(extractables)
		return [self.tree, self.feature_outputter.features, self.feature_outputter.unique_symbols]

	def generate_features(self, extractables):
		if "ASTREL" in extractables:
			self.generate_ast_inter_relations()
		if "FNAMES" in extractables:
			self.generate_fnames()
		if "FSCOPE" in extractables:
			self.generate_fscope()
		self.feature_outputter.generate_assignments()

	def generate_fnames(self):
		mnode = self.tree
		self.descend_fnames(mnode, [])

	def descend_fnames(self, main_node, outer_funcs):
		nodes = [x for x in ast.walk(main_node)]
		for node in nodes:
			ntype = node.__class__.__name__
			if ntype in ["FunctionDef", "Lambda"]:
				for arg in node.args.args:
					self.feature_outputter.add_multiple_features([node], "FN", arg, "PAR")
				outer_funcs = outer_funcs + [node]
				children = [x for x in ast.walk(node)]
				children = children[1:]
				for child in children:
					self.descend_fnames(child, outer_funcs)
				outer_funcs = outer_funcs[:-1]

			if ntype == "Call":
				self.feature_outputter.add_multiple_features(outer_funcs, "FN", node, "CALL")
			elif ntype == "Attribute":
				self.feature_outputter.add_multiple_features(outer_funcs, "FN", node, "PROP")
			elif ntype == "Assign":
				#TARGETS is a tuple
				for target in node.targets:
					self.feature_outputter.add_multiple_features(outer_funcs, "FN", target, "DECL")
			elif ntype == "Return":
				self.feature_outputter.add_multiple_features(outer_funcs, "FN", node.value, "RETURN")


	def generate_fscope(self):
		nodes = [x for x in ast.walk(self.tree)]
		for node in nodes:
			if node.__class__.__name__ in ["Module", "FunctionDef", "Lambda"]:
				self.feature_outputter.generate_scope_relations(node)

	def generate_ast_inter_relations(self):
		bfw = ast.walk(self.tree)
		for node in bfw:
			paths = search_paths(node, 0)[1:]
			for i in range(len(paths)-1):
				path1 = paths[i]
				for j in range(i+1, len(paths)):
					path2 = paths[j]
					self.update_inferred_feature(path1, path2)

	def update_inferred_feature(self, path1, path2):
		node1 = path1[-1]
		node2 = path2[-1]

		cp_len = 0
		while cp_len < len(path1) and cp_len < len(path2) and path1[cp_len] == path2[cp_len]:
			cp_len += 1
		backward_path = path1 if cp_len != len(path2) else path2
		forward_path = path2 if cp_len != len(path2) else path1
		self.feature_outputter.update_output_dict(node1, node2, backward_path, forward_path, cp_len)

#code = """def flunc(a,b,c):\n\ta1=tt.length.ggg\n\tb.open('GET',a,False)\n\tb.send(spin(c))\n\tfoo = [1,2,3] \n\tbaz = foo.map(lambda n : n + 1 )\n\treturn baz\n\nb=5"""
#fe = FeatureExtractor(code, False)
#fts = fe.extract()
#json.dumps(fts)
