var messagesElt = document.getElementById('messages');

String.prototype.singularize = function(num) {
  return (num == 1 ? this.slice(0, -1) : this);
};

/*
Date.getFormattedDateDiff2 = function(date1, date2, intervals) {
  var b    = moment(date1),
      a    = moment(date2),
      dur  = moment.duration(a.diff(b)),
      out  = [];
 
  intervals.forEach(function(interval) {
    var intervalDuration = dur[interval]();
    //unlike the other getters for duration, weeks are counted
    //as a subset of the days, and are not taken off the days count.
    if (interval == 'days') intervalDuration %= 7;
    out.push(intervalDuration + ' ' +  interval.singularize(intervalDuration));
  });
  
  return out.join(', ');
};
*/
Date.getFormattedDateDiff = function(date1, date2, intervals) {
  var b = moment(date1),
      a = moment(date2),
      intervals = intervals || ['years','months','weeks','days'],
      out = [];

  intervals.forEach(function(interval) {
      var diff = a.diff(b, interval);
      b.add(diff, interval);
      out.push(diff + ' ' + interval.singularize(diff));
  });
  return out.join(', ');
};

function getBirthday(Inputyear, Inputmonth, Inputday) {
   var inputYear  = parseInt(Inputyear.value),
       inputMonth = parseInt(Inputmonth.value),
       inputDay   = parseInt(Inputday.value),
       birthday;
   
   if ( isNaN(inputDay) ) {
      messagesElt.innerText = 'Invalid day.';
      Inputday.focus();
   }
   else if ( isNaN(inputMonth) ) {
     messagesElt.innerText = 'Invalid month.';
     Inputmonth.focus();
   } 
   else if (inputMonth < 1 || inputMonth > 12) {
      messagesElt.innerText = "Month is out of range";
      Inputmonth.focus();
   } 
   else if ( isNaN(inputYear) ) {
     messagesElt.innerText = 'Invalid year.';
     Inputyear.focus();
   }
   else if (inputYear < 1900) {
      messagesElt.innerText = "You're that old!?";
      Inputyear.focus();
   }
   else {
     var tempBirthday = moment([inputYear, inputMonth-1, inputDay]);
     if ( tempBirthday.isValid() ) {
       if ( tempBirthday.diff(new Date(), 'days') < 0 ) {
         birthday = tempBirthday;
       }
       else {
         messagesElt.innerText = "Your birthdate must be in the past!";
         Inputday.focus();
       }
     }
     else if ( tempBirthday.invalidAt(2) ) {  //month overflow
        messagesElt.innerText = "Day is out of range";
        Inputday.focus();
     }
   }
   
   return birthday;
}

function compute(form) {
   messagesElt.innerHTML = '&nbsp;';
  
   var today       = moment(new Date()),
       birthday    = getBirthday(form.Inputyear, form.Inputmonth, form.Inputday);
   
   if (!birthday) return;
   
   form.AgeYMD.value   = Date.getFormattedDateDiff( birthday, today );
   
}
