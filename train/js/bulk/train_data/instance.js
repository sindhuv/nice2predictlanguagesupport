function getInstanceMembers(obj) {
    var res = [];
    for(var m in obj) { res.push(m); }
   
    return res;
}

function getInstanceValues(obj) {
    var res = [];
    if (obj.length) {
        for(var m in obj) { res.push(obj[m]); }
    }
  
    return res;
}

function getAllPropertyNames( obj ) {
    var originalObj = obj,
        props = [{
      'class': 'instance',
      'props': getInstanceMembers(obj)
    }];
  
    obj = Object.getPrototypeOf( obj );
    do {
        props.push({
          'class': obj.constructor.name, 
          'props': Object.getOwnPropertyNames(obj)
        });
        //remove duplicate properties
        var last = props.length - 1;
        //no need to do compare to instance
        if (last > 0) {
          props[last-1].props = props[last-1].props.filter(function(val) {
              return props[last].props.indexOf(val) == -1;
          });
        }
    } while ( obj = Object.getPrototypeOf( obj ) );
    
    props.forEach(function(clazz, index) {
      clazz.props = clazz.props.map(function(prop) {
        //console.log( typeof(this[prop]) );
        var propInfo = {
          'name': prop,
          'type': typeof(this[prop])
        };
        if (index == 0 
            && Array.isArray( originalObj )
            || ['function','object'].indexOf(typeof originalObj) == -1) {
          propInfo.value = originalObj[prop];
        }
        return propInfo;
      }, originalObj);
    });
    return props;
}

function getPropValue(prop, type) {
  var propValue = prop.value === undefined ? '' : prop.value; 
  (type == 'string') && (propValue = '"' + propValue + '"');
  return propValue;
}

function displayProps(props) {
  props.forEach(function(prop, index) {
     document.writeln(prop.class + ':<br>________<br><pre>');
     document.write('  prop,  type');
     index == 0 && document.writeln('  (value)');
     document.writeln('<br>  -------------------');
     for (var p in prop.props) { 
       document.writeln('  ' + prop.props[p].name + ': ' 
                       + prop.props[p].type + '  '
                       + getPropValue(prop.props[p], prop.props[p].type) + '<br>');
     }
     document.writeln('</pre><br><br>');
  });
}

var a = [1,2,3,4]; //new Date();;
a.aNewProp = 'new prop';
var props = getAllPropertyNames(a);
displayProps(props);

var s = 'this is a string';
props = getAllPropertyNames(a);
displayProps(props);


