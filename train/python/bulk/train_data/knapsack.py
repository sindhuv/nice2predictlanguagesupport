#  k n a p s a c k . p y
#
#  Chris Meyers. 02/17/2014
#

import spt

maxwgt = 10
vals = [0,10,40,30,50]
wgts = [0, 5, 4, 6, 3]

view = spt.View()
view.banner = "Knapsack Problem"
headers=[str(i)+" kg" for i in range(maxwgt+1)]

wgtVals = spt.Matrix(len(vals),3)
wgtVals.title = "<h4>Sack holds %s kg.</h4>" % maxwgt
wgtVals.dftFormat = "<pre>%03s</pre>"
wgtVals.tableAttr = 'border="1" cellspacing="0" cellpadding="4"',
wgtVals.tableHeaders=['Item #','Kg','Value']             
for i in range(len(vals)) :
    wgtVals.setrowVals(i, [i, wgts[i], vals[i]])

frame = spt.Matrix(1,2)   # for framing weight/value table and optimum table
frame[0,0] = wgtVals.renderHtml()
nItems = len(vals)
best = spt.Matrix(nItems,maxwgt+1)
best.dftFormat = "<pre>%03s</pre>"

title = "<h4>Maximum value for %s kg from items 1-%s includes item %s</h4>"

for i in range(1,nItems) :
    best.setrowVal(i,0)

for i in range(1,nItems) :
    for w in range(0,maxwgt+1) :
        remBest = best[i-1,w-wgts[i]]
        if remBest == None : remBest = 0
        newSolution = vals[i]+remBest
        if ((wgts[i] <= w and newSolution > best[i-1,w])) :
            best[i,w] = newSolution
            best.style[i,w] = "background-color:pink"
            best.title = title % (w,i,i)
            best.tableAttr='border="1" cellspacing="0" cellpadding="4"'
            best.tableHeaders=headers
            frame[0,1] = best.renderHtml()
            view.item1 = frame.renderHtml()
            view.snapshot()
        else :
            best[i,w] = best[i-1,w]

#  Walk backwards to reconstruct path from optimums
best.title = "<h4>Now we reconstruct actual items to use (green)</h4>"
frame[0,1] = best.renderHtml()
view.item1 = frame.renderHtml()
view.snapshot()

item  = len(vals)-1
avail = maxwgt

title = '<h4>Solution for "%s kg from items 1-%s" %s item %s</h4>'
while avail and item > 0 :
    if best[item,avail] :
        iWgt = wgts[item]
        colored = best.style[item,avail]
        if colored and "pink" in colored : 
            best.title = title % (avail,item,"keeps",item)
            best.style[item,avail] = "background-color:lightgreen"
            avail -= iWgt
        else :
            best.title = title % (avail,item,"drops",item)
            best.style[item,avail] = "background-color:yellow"
        frame[0,1] = best.renderHtml()
        view.item1 = frame.renderHtml()
        view.snapshot()
    item -= 1

best.title = "<h4>Final solution in green leaves space for %s kg more</h4>" % avail
frame[0,1] = best.renderHtml()
view.item1 = frame.renderHtml()
view.snapshot()
