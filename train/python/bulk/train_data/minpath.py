#  m i n p a t h . p y
#
#  Chris Meyers. 02/15/2014
#

import spt, random
view = spt.View()
view.banner = "Minimum Path from top to bottom"

def renderTriangle(t) :
    t.dftFormat = "<pre>%03s</pre>"
    t.tableAttr='border="0" cellspacing="0" cellpadding="4"'
    return t.renderHtml()

def minpath(nrows) :
    random.seed(113)
    mat = spt.Matrix(1,2)
    tri = spt.Matrix(nrows,nrows*2)
    org = spt.Matrix(nrows,nrows*2)
    offset = nrows-1
    for r in range(nrows) :
        col = offset
        for i in range(r+1) :
            val = random.randint(1,9)
            org[r,offset+i*2] = val
            tri[r,offset+i*2] = val
        offset -= 1
  
    org.title = "Individual costs at each step"
    mat[0,0] = renderTriangle(org)
    mat[0,1] = renderTriangle(tri)
    mat.tableAttr='border="1" cellspacing="0" cellpadding="4"'

    offset = 1
    for r in range(nrows-2,-1,-1) :
        for c in range(offset,offset+2+r*2,2) :
            tri[r,c] += min(tri[r+1,c-1],tri[r+1,c+1])
            tri.style[r,c] = "background-color:yellow"
            if r == 0 : tri.title = "Miniumum cost is %s" % tri[r,c]
            mat[0,1] = renderTriangle(tri)
            view.item1 = mat.renderHtml()
            view.snapshot()
        offset += 1
        
minpath(7)
