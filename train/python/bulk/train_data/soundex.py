# soundex.py

groups = ['aehiouwy','bfpv','cgjkqsxz','dt','l','mn','r']

# Make dictionary numerics to map each letter to its group
numerics = {}
for i in range(len(groups)) :
    group = groups[i]
    for letter in group :
        numerics[letter] = str(i)  # set to '0' to '6'

def soundex(word) :
    "Return 4 character soundex code for word passed"
    prev = None
    word = word.lower()
    code = squeezed = word[:1]     # 1st letter always kept
    prev = numerics.get(code,None)
    if not prev : return "0000"    # empty or not real word
    for ch in word[1:] :
        nch = numerics.get(ch,'0') # get numeric code
        if nch != '0' and nch != prev :   # keeper ?
            if len(code) < 4 :     # only up to 4 chars
                code += nch
                squeezed += ch     # squeezed - letters used
                prev  = nch
            else : break
    code += '0'*(4-len(code))      # pad '0' to len 4
    return code.upper(), squeezed

def test() :
    import sys, re
    if sys.argv[1:] :   # words on the command line ?
        for word in sys.argv[1:] :
            print word, soundex(word.lower())
    else :
        # read stdin line by line and soundex each word
        while True :
            line = sys.stdin.readline()
            if not line : break
            # strip puncuation, set all to lower case, set words to array
            #  note. dash and single quote are valid characters
            words = re.sub("[^a-zA-Z'-]"," ",line).lower().split()
            for word in words :
                code, squeezed = soundex(word.lower())
                print "%4s %-4s %s" % (code, squeezed, word)

if __name__ == "__main__" : test()
