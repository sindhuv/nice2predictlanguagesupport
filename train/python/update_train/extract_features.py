#!/usr/bin/python
# Adapted from https://github.com/eth-srl/UnuglifyJS/blob/master/extract_features.py

import multiprocessing
import os, json
import sys
import shutil
scriptpath = "../../../src/n2p_python"
sys.path.append(os.path.abspath(scriptpath))
from feature_extractor import *

def PrintUsage():
  print """
Usage:
  extract_features.py --filelist <file>
OR
  extract_features.py --dir <directory>
"""
  exit(1)

def GetPythonFilesInDir(d):
  for root, _, files in os.walk(d):
    for f in files:
      fname = os.path.join(root, f)
      if fname.endswith('.py'):
        yield fname


TMP_DIR = ""

def ExtractFeaturesForFile(input_f):
  #print "============================\n", input_f
  code = ""
  with open(input_f, "r") as f:
    code = f.read() 
  fe = FeatureExtractor(code)
  fts = fe.extract()[1]
  return json.dumps(fts)

def ExtractFeaturesForFileList(files):
  global TMP_DIR
  TMP_DIR = "/tmp/feature_extractor%d" % (os.getpid())
  if os.path.exists(TMP_DIR):
    shutil.rmtree(TMP_DIR)
  os.makedirs(TMP_DIR)
  try:
    p = multiprocessing.Pool(multiprocessing.cpu_count())
    op = p.map(ExtractFeaturesForFile, files)
    for o in op:
	print o
  finally:
    shutil.rmtree(TMP_DIR)


if __name__ == '__main__':
  if (len(sys.argv) <= 1):
    PrintUsage()

  # Process command line arguments
  if (sys.argv[1] == "--filelist"):
    files = open(sys.argv[2], 'r').read().split('\n')
  elif (sys.argv[1] == "--dir"):
    files = [f for f in GetPythonFilesInDir(sys.argv[2])]
  else:
    PrintUsage()
  ExtractFeaturesForFileList(files)
	

